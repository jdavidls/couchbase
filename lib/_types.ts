import { binding } from './_binding';

export type MutationToken = unknown;

/** */
export enum ConnectionType {
  /** */
  Bucket = binding.LCB_TYPE_BUCKET,
  /** */
  Cluster = binding.LCB_TYPE_CLUSTER
}

/**
 * 
 */
export enum DurabilityLevel {

  /** */
  None = binding.LCB_DURABILITYLEVEL_NONE,

  /** */
  Majority = binding.LCB_DURABILITYLEVEL_MAJORITY,

  /** */
  MajorityAndPersistToActive = binding.LCB_DURABILITYLEVEL_MAJORITY_AND_PERSIST_TO_ACTIVE,

  /** */
  PersistToMajority = binding.LCB_DURABILITYLEVEL_PERSIST_TO_MAJORITY,
}

/** */
export enum StoreOperation {
  Upsert = binding.LCB_STORE_UPSERT,
  Insert = binding.LCB_STORE_INSERT,
  Replace = binding.LCB_STORE_REPLACE,
  Append = binding.LCB_STORE_APPEND,
  Prepend = binding.LCB_STORE_PREPEND,
/*
  LCB_STORE_UPSERT: 0;
  LCB_STORE_REPLACE: 2;
  LCB_STORE_INSERT: 1;
  LCB_STORE_APPEND: 4;
  LCB_STORE_PREPEND: 5;  
  Upsert = "upsert",
  Insert = "insert",
  Replace = "replace",
  // Append = "append", only for binary documents, binary/collections
  // Prepend = "prepend"
*/  
}

/** @internal */
export enum ResponseFlags {
  Final = binding.LCB_RESP_F_FINAL,
  ClientGen = binding.LCB_RESP_F_CLIENTGEN,
  NMVGen = binding.LCB_RESP_F_NMVGEN,
  ExtData = binding.LCB_RESP_F_EXTDATA,
  SDSingle = binding.LCB_RESP_F_SDSINGLE,
  ErrInfo = binding.LCB_RESP_F_ERRINFO,
}

/** @internal */
export enum QueryFlags {
  PrepCache = binding.LCBX_QUERYFLAG_PREPCACHE,
}

/** @internal */
export enum ViewQueryFlags {
  IncludeDocs = binding.LCBX_VIEWFLAG_INCLUDEDOCS,
}


/**
 * 
 */
export enum QueryScanConsistency {
  /** */
  NotBounded = 'not_bounded',
  /** */
  RequestPlus = 'request_plus',
  // AtPlus = 'at_plus', not implemented
}

/**
 * 
 */
export type TimeoutOption = { timeout: number };

/**
 * 
 */
export type DurabilityOption = {
  /** */
  durabilityLevel: DurabilityLevel;

  /** */
  replicaTo: number;

  /** */
  persistTo: number;
}

/**
 * 
 */
export interface CASOption { cas: string | Buffer }

export type ExpirationOption = { expiration: number | Date }

export type LocktimeOption = { locktime: number };

export type StoreOperationOption = { operation: StoreOperation };

export type QueryReadonlyOption = { readonly: boolean };

export type QueryParametersOption = { parameters: { [k: string]: any } | any[] };

export type QueryAdhocOption = { adhoc: boolean };

export type QueryScanConsistencyOption = { scanConsistency: QueryScanConsistency };


export type ExecutorOption<ExecutorResult, Payload> = { executor: Executor<ExecutorResult, Payload> };

export type QueryExecutorOption<ExecutorResult, Payload, Metadata> = { executor: QueryExecutor<ExecutorResult, Payload, Metadata> };

// export type ViewQueryStaleOption = { stale: ViewQueryStale };


export type Executor<ExecutorResult, Payload> = (trigger: ExecutorTriggerFn<Payload>) => ExecutorResult;
export type ExecutorTriggerFn<Payload> = (
  value: (item: Payload) => void,
  error: (err: any) => void
) => void;


export type QueryExecutor<ExecutorResult, Payload, Metadata> = (trigger: QueryExecutorTriggerFn<Payload, Metadata>) => ExecutorResult;
export type QueryExecutorMetadataFn<Metadata> = () => Metadata;
export type QueryExecutorTriggerFn<Payload, Metadata> = (
  next: (row: Payload) => void,
  error: (err: any) => void,
  complete: (getMetadata: QueryExecutorMetadataFn<Metadata>) => void
) => void;




import { Connection } from './Connection';
import { ConnectionType } from './_types';

/**
 * @internal
 */
export type ProvidedConnection = {
  connection: Connection | null | false;
  toBeStablished: Promise<Connection>;
};

/**
 * @internal
 */
export class ConnectionProvider {

  private providedClusterConnection: ProvidedConnection | null = null;
  private _bucketConnections = new Map<string, ProvidedConnection>();

  constructor(
    private readonly _clusterUrl: URL,
    private readonly _logger = null) {
  }

  _closeAllConnections() {
    this._closeAllBucketConnections();
    this._closeClusterConnection();
  }

  private _openConnection(bucketName?: string): ProvidedConnection {
    const { _logger } = this;
    const { hostname, username, password } = this._clusterUrl;

    const providedConnection: ProvidedConnection = {
      connection: null,
      toBeStablished: connect(3)
    };

    return providedConnection;

    function connect(retry: number): Promise<Connection> {
      return new Promise<Connection>((resolve, reject) => {

        const connection = new Connection(
          bucketName ? ConnectionType.Bucket : ConnectionType.Cluster,
          bucketName ? hostname : `${hostname}/${bucketName}`,
          username,
          password,
          _logger);

        connection.connect(err => err ?
          reject(err) :
          resolve(connection))
      })
        .then(
          connection => providedConnection.connection = connection,
          error => {
            if (retry > 0) {
              // console.warn(`Retrying to establish connection`);
              return connect(retry - 1);
            }
            else {
              providedConnection.connection = false;
              // console.error(`Error establishing connection`);
              throw error;
            }
          }
        );
    }

  }

  _provideClusterConnection(): ProvidedConnection {
    let provided = this.providedClusterConnection;
    if (!provided || provided.connection === false) {
      this.providedClusterConnection = provided = this._openConnection();
    }
    return provided;
  }

  _closeClusterConnection() {
    const provided = this.providedClusterConnection;
    if (provided && provided.connection) {
      provided.connection.shutdown();
      this.providedClusterConnection = null
    }
  }

  _getBucketConnection(bucketName: string): ProvidedConnection {
    const connections = this._bucketConnections;

    let provided = connections.get(bucketName);
    if (!provided || provided.connection === false) {
      provided = this._openConnection(bucketName);
      connections.set(bucketName, provided);
    }

    return provided;
  }

  _closeAllBucketConnections() {
    const { _bucketConnections } = this;
    for (const { connection } of _bucketConnections.values()) {
      if (connection)
        connection.shutdown();
    }
    _bucketConnections.clear();
  }

}

import qs from 'qs';
import { ConnectionProvider } from './ConnectionProvider';
import { AsyncQueryExecutor } from './executors/AsyncExecutor';
import { Scope } from './Scope';
import { QueryExecutor, QueryExecutorOption, ResponseFlags, TimeoutOption, ViewQueryFlags } from './_types';

/**
 * 
 */
export enum ViewQueryStale {
  /** */
  Ok = 'ok',

  /** */
  False = 'false',

  /** */
  UpdateAfter = 'update_after',

}

/**
 * 
 */
export type ViewQueryMetadata = {

};

/**
 * 
 */
export type ViewQueryPayload = {
  id: string | null;
  key: string[];
  value: any;
}
// export type ViewQueryExecutor<ExecutorResult> = QueryExecutor<ExecutorResult, ViewQueryPayload, ViewQueryMetadata>;
// export type ViewQueryExecutorOption<ExecutorResult> = QueryExecutorOption<ExecutorResult, ViewQueryPayload, ViewQueryMetadata>;

export type ViewQueryOptions<ExecutorResult> = Partial<
  QueryExecutorOption<ExecutorResult, ViewQueryPayload, ViewQueryMetadata> &
  TimeoutOption & {
    stale: ViewQueryStale,
    skip: number,
    limit: number,
    reversed: boolean,
    reduce: string,

    group: boolean, groupLevel: number,

    key: string[],
    keys: string[][],

    keyRange: { start: string[], end: string[] },
    idRange: { start: string[], end: string[] },
    inclusive: boolean,

    includeDocs: boolean,
    fullSet: boolean,
    onError: unknown,
  }>;


type ViewQueryObject = {
  stale?: ViewQueryStale,
  skip?: number,
  limit?: number,
  descending?: boolean,
  reduce?: string,

  group?: boolean, group_level?: number,

  key?: string,
  keys?: string

  startkey?: string;
  endkey?: string;

  startkey_docid?: string;
  endkey_docid?: string;

  inclusive_end?: boolean;

  include_docs?: boolean,
  full_set?: boolean,
  on_error?: unknown,
};

export class Bucket extends Scope {
  constructor(
    connectionProvider: ConnectionProvider,
    bucketName?: string) {
    super(connectionProvider, bucketName);
  }

  scope(scopeName: string) {
    return new Scope(
      this.connectionProvider,
      this.bucketName,
      scopeName);
  }

  viewQuery<ExecutorResult = Promise<any[]>>(
    designDocName: string,
    viewName: string,
    options: ViewQueryOptions<ExecutorResult> = {}):
    ExecutorResult {

    const queryObject: ViewQueryObject = {
      stale: options.stale,
      skip: options.skip,
      limit: options.limit,
      descending: options.reversed,
      reduce: options.reduce,
      group: options.group,
      group_level: options.groupLevel,
      key: options.key && JSON.stringify(options.key),
      keys: options.keys && JSON.stringify(options.keys),
      startkey: options.keyRange && JSON.stringify(options.keyRange.start),
      endkey: options.keyRange && JSON.stringify(options.keyRange.end),
      startkey_docid: options.idRange && JSON.stringify(options.idRange.start),
      endkey_docid: options.idRange && JSON.stringify(options.idRange.end),
      inclusive_end: options.inclusive,
      full_set: options.fullSet,
      on_error: options.onError
    };

    const queryFlags = options.includeDocs ? ViewQueryFlags.IncludeDocs : 0;
    const timeout = this._pickTimeoutOption(options);
    const executor = options.executor || AsyncQueryExecutor as any as QueryExecutor<ExecutorResult, ViewQueryPayload, ViewQueryMetadata>;

    const queryString = qs.stringify(queryObject);

    return executor((next, error, complete) => {

      this._getConnection()
        .then(connection => connection.viewQuery(
          designDocName, 
          viewName, 
          queryString, 
          null, 
          queryFlags,
          timeout, 
          (err, flags, data, key, id) => {
            try {
              if (err) {
                error(err);
              }
              else if (flags & ResponseFlags.Final) {
                complete(() => _parseViewQueryMetadata(data));
              }
              else {
                next({
                  id: String(id) || null,
                  key: JSON.parse(String(key)),
                  value: JSON.parse(String(data)),
                });
              }
            }
            catch (err) {
              error(err);
            }
    
          }
        ))
        .catch(err => error(err));
    });

  }


}

function _parseViewQueryMetadata(meta: Buffer) {
  return JSON.parse(String(meta));
}
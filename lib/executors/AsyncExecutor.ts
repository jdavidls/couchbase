import { ExecutorTriggerFn, QueryExecutorTriggerFn } from "../_types";


export function AsyncExecutor<Payload>(run: ExecutorTriggerFn<Payload>) {
  return new Promise<Payload>((resolve, reject) => {
    run(
      row => resolve(row),
      err => reject(err));
  })
}

export function AsyncQueryExecutor<Payload, Metadata>(runQuery: QueryExecutorTriggerFn<Payload, Metadata>) {
  return new Promise<Payload[]>((resolve, reject) => {
    const rows: Payload[] = [];
    runQuery(
      row => rows.push(row),
      err => reject(err),
      () => resolve(rows));
  })
}

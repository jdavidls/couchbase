
import { Observable } from 'rxjs';
import { ExecutorTriggerFn, QueryExecutorTriggerFn } from '../_types';

export function RXExecutor<Payload>(runQuery: ExecutorTriggerFn<Payload>) {
  return new Observable<any>(sink => {
    runQuery(
      val => {
        sink.next(val);
        sink.complete();
      },
      err => sink.error(err),
    );
  })
}

export function RXQueryExecutor<Payload, Metadata>(runQuery: QueryExecutorTriggerFn<Payload, Metadata>) {
  return new Observable<any>(sink => {
    runQuery(
      row => sink.next(row),
      err => sink.error(err),
      () => sink.complete()
    );
  })
}

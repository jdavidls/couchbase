
import { Transcoder } from './Transcoder';
import { binding } from './_binding';
import { ResponseFlags, DurabilityLevel, MutationToken, StoreOperation, ConnectionType, ViewQueryFlags } from './_types';


export type ConnectionLoggerFn = () => void;

export type ConnectionCtor = new (
  type: ConnectionType,
  path: string,
  username: string,
  password: string,
  logger?: ConnectionLoggerFn | null) => ConnectionPrototype;

export interface ConnectionPrototype {
  connect(
    callback: (err: any) => void
  ): void;

  selectBucket(
    buckerName: string,
    callback: (err: any) => void
  ): void;

  shutdown(): true;

  cntl(): void;

  get(
    scope: string,
    collection: string,
    key: string | Buffer,
    transcoder: Transcoder,
    expiration: number,
    locktime: number,
    timeout: number,
    callback: (
      err: any,
      cas: Buffer,
      val: any) => void
  ): void;

  exists(): void;

  getReplica(): void;

  store(
    scope: string,
    collection: string,
    key: string | Buffer,
    transcoder: Transcoder,
    value: any,
    expiration: number,
    cas: string | Buffer | undefined,
    durabilityLevel: DurabilityLevel,
    persistTo: number,
    replicateTo: number,
    timeout: number,
    operation: StoreOperation,
    callback: (
      err: any,
      cas: Buffer,
      mut: MutationToken) => void
  ): void;

  remove(
    scope: string,
    collection: string,
    key: string | Buffer,
    cas: string | Buffer | undefined,
    durabilityLevel: DurabilityLevel,
    persistTo: number,
    replicateTo: number,
    timeout: number,
    callback: (
      err: any,
      cas: Buffer,
      val: any) => void
  ): void;


  touch(): void;

  unlock(): void;

  counter(): void;

  lookupIn(): void;

  mutateIn(): void;

  viewQuery(
    viewDocName: string,
    viewName: string,
    getString: string,
    postString: string | null,
    flags: ViewQueryFlags,
    timeout: number,
    callback: (
      err: any,
      flags: ResponseFlags,
      data: Buffer,
      key: Buffer,
      id: Buffer) => void
  ): void;

  query(
    queryString: string,
    flags: number,
    timeout: number,
    callback: (
      err: any,
      flags: ResponseFlags,
      data: Buffer) => void
  ): void;

  analyticsQuery(): void;

  searchQuery(): void;

  httpRequest(): void;

  ping(): void;

  diag(): void;

};

export class Connection extends binding.Connection {
}

import { Collection } from "./Collection";
import { ConnectionProvider } from "./ConnectionProvider";

export class Scope extends Collection {
  constructor(
    connectionProvider: ConnectionProvider,
    bucketName?: string, scopeName?: string) {

    super(connectionProvider, bucketName, scopeName);
  }

  collection(collectionName: string) {
    return new Collection(
      this.connectionProvider, 
      this.bucketName,
      this.scopeName, 
      collectionName);
  }
}

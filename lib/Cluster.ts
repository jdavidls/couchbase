import { Bucket } from './Bucket';
import { ConnectionProvider } from './ConnectionProvider';
import { AsyncQueryExecutor } from './executors/AsyncExecutor';
import { QueryAdhocOption, QueryExecutor, QueryExecutorOption, QueryFlags, QueryParametersOption, QueryReadonlyOption, QueryScanConsistency, QueryScanConsistencyOption, ResponseFlags, TimeoutOption, ExecutorOption } from './_types';
import { Connection } from './Connection';

export type ConnectOptions = Partial<
  ExecutorOption<Promise<Connection>, Connection>
  // LoggerOption<>
  >;

export type QueryMetadata = {

};


export type QueryOptions<ExecutorResult> = Partial<
  QueryExecutorOption<ExecutorResult, any, QueryMetadata> &
  QueryReadonlyOption &
  QueryParametersOption &
  QueryAdhocOption &
  QueryScanConsistencyOption &
  TimeoutOption>;

type QueryObject = {
  statement: string;
  args: { [k: string]: any } | any[];
  readonly: boolean;
  scan_consistency?: QueryScanConsistency;

  scan_vectors?: unknown;
  client_context_id?: unknown;
  max_parallelism?: unknown;
  pipeline_batch?: unknown;
  pipeline_cap?: unknown;
  scan_wait?: unknown;
  scan_cap?: unknown;
  profile?: unknown;
  metrics?: unknown;
};

/**
 * 
 */
export class Cluster extends Bucket {
  /**
   * 
   * @param url 
   */
  static connect(url: URL | string) {
    if (typeof url === 'string')
      url = new URL(url);

    return new Cluster(new ConnectionProvider(url))
  }


  constructor(connectionProvider: ConnectionProvider) {
    super(connectionProvider);
  }

  /**
   * 
   */
  close() {
    this.connectionProvider._closeAllConnections();
  }

  /**
   * 
   * @param bucketName 
   */
  bucket(bucketName: string) {
    return new Bucket(this.connectionProvider, bucketName);
  }

  /**
   * 
   * @param statement The n1ql statement to execute
   * @param options 
   */

  query<Payload, ExecutorResult = Promise<any[]>>(
    statement: string,
    options: QueryOptions<ExecutorResult> = {}):
    ExecutorResult {

    const queryObject: QueryObject = {
      statement,
      args: this._pickQueryParametersOption(options),
      readonly: this._pickQueryReadonlyOption(options),
      scan_consistency: this._pickQueryScanConsistencyOption(options),
    };

    const queryFlags = 0 |
      this._flagsOfQueryAdhocOption(options)
      ;
    const timeout = this._pickTimeoutOption(options);
    const executor = options.executor || AsyncQueryExecutor as any as QueryExecutor<ExecutorResult, Payload, QueryMetadata>;
    const queryString = JSON.stringify(queryObject);

    return executor((next, error, complete) => {

      this._getConnection()
        .then(connection => connection.query(
          queryString, 
          queryFlags, 
          timeout, 
          (err, flags, data) => {
            try {
              if (err) {
                error(err);
              }
              else if (flags & ResponseFlags.Final) {
                complete(() => _parseQueryMetadata(data));
              }
              else {
                next(JSON.parse(String(data)));
              }
            }
            catch (err) {
              error(err);
            }
          }
        ))
        .catch(err => error(err));
    });
  }

  private _pickQueryParametersOption(options: Partial<QueryParametersOption>) {
    const { parameters } = options;
    if (parameters) {
      if (Array.isArray(parameters)) {
        return parameters;
      }
      else {
        const obj: any = {};
        for (const k in parameters) {
          obj[k] = parameters[k];
        }
        return obj;
      }
    }
  }
  /*
    private _pickExecutorOption<QR>(options: Partial<ExecutorOption<QR>>) {
      return options.executor || AsyncQueryExecutor;
    }
  */
  private _flagsOfQueryAdhocOption(options: Partial<QueryAdhocOption>) {
    return !!options.adhoc ? 0 : QueryFlags.PrepCache;
  }

  private _pickQueryReadonlyOption(options: Partial<QueryReadonlyOption>) {
    return !!options.readonly;
  }

  private _pickQueryScanConsistencyOption(options: Partial<QueryScanConsistencyOption>) {
    return options.scanConsistency || undefined;
  }

}

function _parseQueryMetadata(rawMeta: Buffer): QueryMetadata {
  const meta = JSON.parse(String(rawMeta));
  // TODO: transformations..
  return meta;
}


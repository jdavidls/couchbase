import { Connection } from './Connection';
import { ConnectionProvider } from './ConnectionProvider';
import { defaultTranscoder, Transcoder } from './Transcoder';
import { CASOption, DurabilityLevel, DurabilityOption, ExpirationOption, LocktimeOption, MutationToken, StoreOperation, StoreOperationOption, TimeoutOption, ExecutorOption, Executor } from './_types';
import { AsyncExecutor } from './executors/AsyncExecutor';

/**
 * 
 */
export type GetPayload = {
  cas: Buffer;
  value: any;
}

/**
 * 
 */
export type GetOptions<ExecutorResult> = Partial<
  ExecutorOption<ExecutorResult, GetPayload> &
  ExpirationOption &
  LocktimeOption &
  TimeoutOption>;


/**
 * 
 */
export type RemoveOptions<ExecutorResult> = Partial<
  ExecutorOption<ExecutorResult, void> &
  CASOption &
  DurabilityOption &
  TimeoutOption>;


/**
 * 
 */

export type StorePayload = {
  cas: Buffer;
  mutToken: MutationToken;
};

export type StoreOptions<ExecutorResult> = Partial<
  ExecutorOption<ExecutorResult, StorePayload> &
  StoreOperationOption &
  CASOption &
  ExpirationOption &
  DurabilityOption &
  TimeoutOption>;


/**
 * 
 */
export class Collection {

  constructor(
    readonly connectionProvider: ConnectionProvider,
    readonly bucketName: string = 'default',
    readonly scopeName: string = '_default',
    readonly collectionName: string = '_default') {
  }

  /**
   * 
   * @param key 
   * @param options 
   */

  get<ExecutorResult = Promise<GetPayload>>(
    key: string | Buffer,
    options: GetOptions<ExecutorResult> = {}):
    ExecutorResult {

    const { scopeName, collectionName } = this;
    const transcoder = this._getTranscoder();
    const expiration = this._pickExpirationOption(options);
    const locktime = this._pickLocktimeOption(options);
    const timeout = this._pickTimeoutOption(options);
    const executor = options.executor || AsyncExecutor as any as Executor<ExecutorResult, GetPayload>;

    return executor((resolve, reject) => {
      this._getConnection()
        .then(connection => connection.get(
          scopeName, 
          collectionName, 
          key, 
          transcoder, 
          expiration, 
          locktime, 
          timeout,
          (err, cas, value) => err ?
            reject(err) :
            resolve({ cas, value })
        ))
        .catch(err => reject(err));
    })

  }

  /**
   * 
   * @param key 
   * @param options 
   */
  remove<ExecutorResult = Promise<void>>(
    key: string | Buffer,
    options: RemoveOptions<ExecutorResult> = {}):
    ExecutorResult {

    const { scopeName, collectionName } = this;
    const cas = this._pickCasOption(options);
    const { durabilityLevel, persistTo, replicaTo } = this._pickDurabilityOption(options);
    const timeout = this._pickTimeoutOption(options);
    const executor = options.executor || AsyncExecutor as any as Executor<ExecutorResult, void>;

    return executor((resolve, reject) => {
      this._getConnection()
        .then(connection => connection.remove(
          scopeName, 
          collectionName, 
          key, 
          cas, 
          durabilityLevel, 
          persistTo, 
          replicaTo, 
          timeout,
          (err) => err ?
            reject(err) :
            resolve()
        ))
        .catch(err => reject(err));
    });

  }

  /**
   * 
   * @param key 
   * @param value 
   * @param options 
   */

  store<ExecutorResult = Promise<StorePayload>>(
    key: string | Buffer,
    value: any,
    options: StoreOptions<ExecutorResult> = {}):
    ExecutorResult  {

    const { scopeName, collectionName } = this;
    const transcoder = this._getTranscoder();
    const expiration = this._pickExpirationOption(options);
    const expectedCAS = this._pickCasOption(options);
    const { durabilityLevel, persistTo, replicaTo, } = this._pickDurabilityOption(options);
    const timeout = this._pickTimeoutOption(options);
    const operation = options.operation || StoreOperation.Upsert;
    const executor = options.executor || AsyncExecutor as any as Executor<ExecutorResult, StorePayload>;

    return executor((resolve, reject) => {
      this._getConnection()
        .then(connection => connection.store(
          scopeName, 
          collectionName, 
          key, 
          transcoder, 
          value, 
          expiration,
          expectedCAS, 
          durabilityLevel, 
          persistTo, 
          replicaTo, 
          timeout, 
          operation,
          (err, cas, mutToken) => err ?
            reject(err) :
            resolve({ cas, mutToken })))
        .catch(err => reject(err));
    });

  }

  protected _getConnection(): Promise<Connection> {
    return this.connectionProvider
      ._getBucketConnection(this.bucketName)
      .toBeStablished;
  }

  protected _getTranscoder(): Transcoder {
    return defaultTranscoder;
  }

  protected _pickTimeoutOption(options: Partial<TimeoutOption>): number {
    return options.timeout || 0;
  }
  protected _pickExpirationOption(options: Partial<ExpirationOption>): number {
    return options.expiration ? +options.expiration : 0;
  }

  protected _pickLocktimeOption(options: Partial<LocktimeOption>): number {
    return options.locktime || 0;
  }

  protected _pickDurabilityOption(options: Partial<DurabilityOption>): DurabilityOption {
    return {
      durabilityLevel: options.durabilityLevel || DurabilityLevel.None,
      replicaTo: options.replicaTo || 0,
      persistTo: options.persistTo || 0,
    };
  }

  protected _pickCasOption(options: Partial<CASOption>): string | Buffer | undefined {
    return options.cas;
  }

}


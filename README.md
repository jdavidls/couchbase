# Couchbase Node.js Client (non-oficial fork)

The Node.js SDK library allows you to connect to a Couchbase cluster from 
Node.js. It is a native Node.js module and uses the very fast libcouchbase 
library to handle communicating to the cluster over the Couchbase binary
protocol.

__This library is a partial reimplementation of the official couchnode library__

## Installing

To install the lastest release using npm, run:
```bash
npm install couchbase-x
```

## Introduction

Connecting to a Couchbase bucket is as simple as creating a new `Cluster` 
instance to represent the `Cluster` you are using, and then using the
`bucket` and `collection` commands against this to open a connection to
open your specific bucket and collection.  You are able to execute most
operations immediately, and they will be queued until the connection is
successfully established.

Here is a simple example of instantiating a connection, adding a new document
into the bucket and then retrieving its contents:

```typescript
import { Cluster } from 'couchbase-x';

const cluster = Cluster.connect('cb://<username>:<password>@120.0.0.1');

await cluster.store('test-doc', {
  name: 'Frank',
});

const doc = await cluster.get('test-doc');

console.log(doc);
// {name: 'Frank'}
```

## Documentation

An extensive documentation is available on the Couchbase website.  Visit our
[Node.js Community](http://couchbase.com/communities/nodejs) on
the [Couchbase](http://couchbase.com) website for the documentation as well as
numerous examples and samples.


## Source Control

The source code is available at
[https://github.com/couchbase/couchnode](https://github.com/couchbase/couchnode).
Once you have cloned the repository, you may contribute changes through our
gerrit server.  For more details see
[CONTRIBUTING.md](https://github.com/couchbase/couchnode/blob/master/CONTRIBUTING.md).

To execute our test suite, run `make test` from the root directory.

To execute our code coverage, run `make cover` from the root directory.

In addition to the full test suite and full code coverage, you may additionally
execute a subset of the tests which excludes slow-running tests for quick
verifications.  These can be run through `make fasttest` and `make fastcover`
respectively.

## Useful Links

Source - [http://github.com/couchbase/couchnode](http://github.com/couchbase/couchnode)

Bug Tracker - [http://www.couchbase.com/issues/browse/JSCBC](http://www.couchbase.com/issues/browse/JSCBC)

Couchbase Developer Portal - [https://docs.couchbase.com/](https://docs.couchbase.com/nodejs-sdk/2.6/start-using-sdk.html)

Release Notes - [https://docs.couchbase.com/nodejs-sdk/2.6/relnotes-nodejs-sdk.html](https://docs.couchbase.com/nodejs-sdk/2.6/relnotes-nodejs-sdk.html)


## License
Copyright 2013 Couchbase Inc.

Licensed under the Apache License, Version 2.0.

See
[LICENSE](https://github.com/couchbase/couchnode/blob/master/LICENSE)
for further details.
